using ChallongeSharp.Clients;
using ChallongeSharp.Clients.Interfaces;

namespace ChallongeSharp
{
    public interface IChallongeConnection
    {
        ITournamentClient Tournament { get; }
        IParticipantClient Participant { get; }
        IMatchClient Match { get; }
    }
}