namespace ChallongeSharp.Models.ViewModels.Types
{
    public class TournamentType
    {
        public TournamentType(string value)
        {
            Value = value;
        }

        public string Value { get; set; }

        public static TournamentType SingleElimination => new TournamentType("single_elimination");
        public static TournamentType DoubleElimination => new TournamentType("double_elimination");
        public static TournamentType RoundRobin => new TournamentType("round_robin");
        public static TournamentType Swiss => new TournamentType("swiss");

        public override string ToString()
        {
            return Value;
        }
    }
}