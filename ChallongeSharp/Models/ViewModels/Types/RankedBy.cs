namespace ChallongeSharp.Models.ViewModels.Types
{
    public class RankedBy
    {
        public RankedBy(string value)
        {
            Value = value;
        }

        public string Value { get; set; }

        public static RankedBy MatchWins => new RankedBy("match wins");
        public static RankedBy GameWins => new RankedBy("game wins");
        public static RankedBy PointsScored => new RankedBy("points scored");
        public static RankedBy PointsDifference => new RankedBy("points difference");

        public override string ToString()
        {
            return Value;
        }
    }
}