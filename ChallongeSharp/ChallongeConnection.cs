﻿using ChallongeSharp.Clients;
using ChallongeSharp.Clients.Interfaces;

namespace ChallongeSharp
{
    public class ChallongeConnection : IChallongeConnection
    {
        public ChallongeConnection(ITournamentClient tournament, IParticipantClient participant, IMatchClient match)
        {
            Tournament = tournament;
            Participant = participant;
            Match = match;
        }

        public ITournamentClient Tournament { get; }
        public IParticipantClient Participant { get; }
        public IMatchClient Match { get; }
    }
}