using System;
using ChallongeSharp.Clients;
using ChallongeSharp.Clients.Interfaces;
using ChallongeSharp.Models.Configurations;
using Microsoft.Extensions.DependencyInjection;

namespace ChallongeSharp.Helpers
{
    public static class ChallongeClientExtensions
    {
        public static IServiceCollection AddChallongeClient(this IServiceCollection services,
            Action<ChallongeConfigurations> options)
        {
            services.AddOptions();
            services.Configure(options);

            services.AddHttpClient<IChallongeClient, ChallongeClient>();

            services.AddTransient<IChallongeConnection, ChallongeConnection>();
            services.AddTransient<IChallongeClient, ChallongeClient>();
            services.AddTransient<ITournamentClient, TournamentClient>();
            services.AddTransient<IParticipantClient, ParticipantClient>();
            services.AddTransient<IMatchClient, MatchClient>();

            return services;
        }
    }
}