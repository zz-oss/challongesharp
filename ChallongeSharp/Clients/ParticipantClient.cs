using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ChallongeSharp.Clients.Interfaces;
using ChallongeSharp.Helpers;
using ChallongeSharp.Models.ChallongeModels;
using ChallongeSharp.Models.ViewModels;

namespace ChallongeSharp.Clients
{
    public class ParticipantClient : IParticipantClient
    {
        private readonly IChallongeClient _client;

        public ParticipantClient(IChallongeClient client)
        {
            _client = client;
        }

        #region GetRequests

        public async Task<List<Participant>> GetAllParticipantsAsync(string tournamentName)
        {
            var requestUrl = $"tournaments/{tournamentName}/participants.json";
            
            var participants = await _client.GetAsync<List<ParticipantResponse>>(requestUrl);
            return participants.Select(p => p.Participant).ToList();
        }

        public async Task<Participant> GetParticipantAsync(string tournamentName, string participantId)
        {
            var requestUrl = $"tournaments/{tournamentName}/participants/{participantId}.json";

            var participant = await _client.GetAsync<ParticipantResponse>(requestUrl);
            return participant.Participant;
        }

        #endregion

        #region PostRequests

        public async Task<Participant> AddParticipantAsync(ParticipantVm participantVm, string tournamentName)
        {
            var requestUrl = $"tournaments/{tournamentName}/participants.json";
            var requestContent = participantVm.ToRequestContent();

            var participant = await _client.PostAsync<ParticipantResponse>(requestUrl, requestContent);
            return participant.Participant;
        }

        public async Task<List<Participant>> BulkAddParticipantsAsync(List<ParticipantVm> participantVms,
            string tournamentName)
        {
            var participants = new List<Participant>();
            foreach (var participant in participantVms)
            {
                participants.Add(await AddParticipantAsync(participant, tournamentName));
            }

            return participants;
        }

        public async Task<Participant> CheckInParticipantAsync(string participantId, string tournamentName)
        {
            var requestUrl = $"tournaments/{tournamentName}/participants/{participantId}/check_in.json";

            var participant = await _client.PostAsync<ParticipantResponse>(requestUrl);
            return participant.Participant;
        }

        public async Task<Participant> UndoCheckInParticipantAsync(string participantId, string tournamentName)
        {
            var requestUrl = $"tournaments/{tournamentName}/participants/{participantId}/undo_check_in.json";

            var participant = await _client.PostAsync<ParticipantResponse>(requestUrl);
            return participant.Participant;
        }

        public async Task<Participant> RandomizeParticipantsAsync(string tournamentName)
        {
            var requestUrl = $"tournaments/{tournamentName}/participants/randomize.json";

            var participant = await _client.PostAsync<ParticipantResponse>(requestUrl);
            return participant.Participant;
        }

        #endregion

        #region PutRequests

        public async Task<Participant> UpdateParticipantAsync(ParticipantVm participantVm, string tournamentName)
        {
            var requestUrl = $"tournaments/{tournamentName}/particpants/{participantVm.Id}.json";

            var participant = await _client.PutAsync<ParticipantResponse>(requestUrl, participantVm);
            return participant.Participant;
        }

        #endregion

        #region DeleteRequests

        public async Task<bool> RemoveParticipantAsync(string participantId, string tournamentName)
        {
            var requestUrl = $"tournaments/{tournamentName}/participants/{participantId}.json";

            var success = await _client.DeleteAsync(requestUrl);
            return success;
        }

        public async Task<bool> ClearAllParticipants(string tournamentName)
        {
            var requestUrl = $"tournaments/{tournamentName}/participants/clear.json";

            var success = await _client.DeleteAsync(requestUrl);
            return success;
        }

        #endregion
    }
}