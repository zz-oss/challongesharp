using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ChallongeSharp.Clients.Interfaces;
using ChallongeSharp.Models.ChallongeModels;
using ChallongeSharp.Models.ViewModels;
using ChallongeSharp.Models.ViewModels.Types;

namespace ChallongeSharp.Clients
{
    public class MatchClient : IMatchClient
    {
        private readonly IChallongeClient _client;

        public MatchClient(IChallongeClient client)
        {
            _client = client;
        }

        public async Task<List<Match>> GetAllMatchesAsync(string tournamentName, TournamentState state = null,
            long? participantId = null)
        {
            var requestUrl = $"tournaments/{tournamentName}/matches.json";
            var options = new MatchOptions 
            {
                State = state,
                ParticipantId = participantId
            };

            var matches = await _client.GetAsync<List<MatchResponse>>(requestUrl, options);
            return matches.Select(m => m.Match).ToList();
        }

        public async Task<Match> GetMatchAsync(string tournamentName, long matchId,
            bool includeAttachments = false)
        {
            var requestUrl = $"tournaments/{tournamentName}/matches/{matchId}.json";
            var options = new MatchOptions
            {
                IncludeAttachments = includeAttachments
            };

            var match = await _client.GetAsync<MatchResponse>(requestUrl, options);
            return match.Match;
        }

        public async Task<Match> UpdateMatchAsync(string tournamentName, long matchId, int player1Score,
            int player2Score, int? player1Votes = null, int? player2Votes = null)
        {
            var match = await GetMatchAsync(tournamentName, matchId);

            var requestUrl = $"tournaments/{tournamentName}/matches/{matchId}.json";
            var options = new MatchOptions
            {
                Player1Score = player1Score,
                Player2Score = player2Score,
                Player1Votes = player1Votes,
                Player2Votes = player2Votes,
                Player1Id = match.Player1Id,
                Player2Id = match.Player2Id
            };

            var updatedMatch = await _client.PutAsync<MatchResponse>(requestUrl, options);
            return updatedMatch.Match;
        }

        public async Task<Match> ReopenMatchAsync(string tournamentName, long matchId)
        {
            var requestUrl = $"tournaments/{tournamentName}/matches/{matchId}/reopen.json";

            var match = await _client.PostAsync<MatchResponse>(requestUrl);
            return match.Match;
        }
    }
}