using System;
using System.Net.Http;
using System.Threading.Tasks;
using ChallongeSharp.Clients.Interfaces;
using ChallongeSharp.Helpers;
using ChallongeSharp.Models.Configurations;
using ChallongeSharp.Models.ViewModels.BaseModels;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;

namespace ChallongeSharp.Clients
{
    public class ChallongeClient : IChallongeClient
    {
        private readonly HttpClient _client;
        private readonly ChallongeConfigurations _config;

        public ChallongeClient(HttpClient client, IOptions<ChallongeConfigurations> config)
        {
            _config = config.Value;
            _client = client;
            _client.BaseAddress = new Uri("https://api.challonge.com");
            _client.DefaultRequestHeaders.Add("Authorization", $"Basic {_config.Token}");
        }

        public async Task<T> GetAsync<T>(string url, BaseModel options)
        {
            if (options != null)
                url += $"?{options.ToRequestParams()}";

            var request = new HttpRequestMessage(HttpMethod.Get, $"/v1/{url}");

            HttpResponseMessage response = await _client.SendAsync(request);
            response.EnsureSuccessStatusCode();

            return JsonConvert.DeserializeObject<T>(await response.Content.ReadAsStringAsync());
        }

        public async Task<T> PostAsync<T>(string url, BaseModel content)
        {
            FormUrlEncodedContent formUrlEncodedContent = content.ToRequestContent();
            var request = new HttpRequestMessage(HttpMethod.Post, $"/v1/{url}") {Content = formUrlEncodedContent};

            HttpResponseMessage response = await _client.SendAsync(request);
            response.EnsureSuccessStatusCode();

            return JsonConvert.DeserializeObject<T>(await response.Content.ReadAsStringAsync());
        }

        public async Task<T> PostAsync<T>(string url, FormUrlEncodedContent content)
        {
            var request = new HttpRequestMessage(HttpMethod.Post, $"/v1/{url}") {Content = content};

            HttpResponseMessage response = await _client.SendAsync(request);
            response.EnsureSuccessStatusCode();

            return JsonConvert.DeserializeObject<T>(await response.Content.ReadAsStringAsync());
        }

        public async Task<T> PostAsync<T>(string url)
        {
            var request = new HttpRequestMessage(HttpMethod.Post, $"/v1/{url}");

            HttpResponseMessage response = await _client.SendAsync(request);
            response.EnsureSuccessStatusCode();

            return JsonConvert.DeserializeObject<T>(await response.Content.ReadAsStringAsync());
        }

        public async Task<T> PutAsync<T>(string url, BaseModel content)
        {
            FormUrlEncodedContent formUrlEncodedContent = content.ToRequestContent();
            var request = new HttpRequestMessage(HttpMethod.Put, $"/v1/{url}") {Content = formUrlEncodedContent};

            HttpResponseMessage response = await _client.SendAsync(request);
            response.EnsureSuccessStatusCode();

            return JsonConvert.DeserializeObject<T>(await response.Content.ReadAsStringAsync());
        }

        public async Task<bool> DeleteAsync(string url)
        {
            var request = new HttpRequestMessage(HttpMethod.Delete, $"/v1/{url}");

            HttpResponseMessage response = await _client.SendAsync(request);
            response.EnsureSuccessStatusCode();

            return response.IsSuccessStatusCode;
        }
    }
}