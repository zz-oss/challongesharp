using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ChallongeSharp.Models.ChallongeModels;
using ChallongeSharp.Models.ViewModels;
using ChallongeSharp.Models.ViewModels.Types;

namespace ChallongeSharp.Clients.Interfaces
{
    public interface ITournamentClient
    {
        Task<List<Tournament>> GetAllTournamentsAsync(TournamentState state = null,
            TournamentType type = null, DateTime? createdAfter = null, DateTime? createdBefore = null,
            string subdomain = null);

        Task<List<Tournament>> GetTournamentAsync(string name, bool includeParticipants = false,
            bool includeMatches = false);

        Task<Tournament> CreateTournamentAsync(TournamentVm tournamentVm);

        Task<Tournament> ProcessCheckInsAsync(string tournamentName, bool includeParticipants = false,
            bool includeMatches = false);

        Task<Tournament> AbortCheckInsAsync(string tournamentName, bool includeParticipants = false,
            bool includeMatches = false);

        Task<Tournament> StartAsync(string tournamentName, bool includeParticipants = false,
            bool includeMatches = false);

        Task<Tournament> FinalizeAsync(string tournamentName, bool includeParticipants = false,
            bool includeMatches = false);

        Task<Tournament> ResetAsync(string tournamentName, bool includeParticipants = false,
            bool includeMatches = false);

        Task<Tournament> OpenForPredictionsAsync(string tournamentName, bool includeParticipants = false,
            bool includeMatches = false);

        Task<Tournament> UpdateTournamentAsync(TournamentVm tournamentVm);
        Task<bool> DeleteTournamentAsync(string tournamentName);
    }
}