using System.Net.Http;
using System.Threading.Tasks;
using ChallongeSharp.Models.ViewModels;
using ChallongeSharp.Models.ViewModels.BaseModels;

namespace ChallongeSharp.Clients.Interfaces
{
    public interface IChallongeClient
    {
        Task<T> GetAsync<T>(string url, BaseModel options = null);
        Task<T> PostAsync<T>(string url, BaseModel content);
        Task<T> PostAsync<T>(string url, FormUrlEncodedContent content);
        Task<T> PutAsync<T>(string url, BaseModel content);
        Task<bool> DeleteAsync(string url);
        Task<T> PostAsync<T>(string url);
    }
}