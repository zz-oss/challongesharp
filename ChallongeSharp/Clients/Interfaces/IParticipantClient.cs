using System.Collections.Generic;
using System.Threading.Tasks;
using ChallongeSharp.Models.ChallongeModels;
using ChallongeSharp.Models.ViewModels;

namespace ChallongeSharp.Clients
{
    public interface IParticipantClient
    {
        Task<List<Participant>> GetAllParticipantsAsync(string tournamentName);
        Task<Participant> GetParticipantAsync(string tournamentName, string participantId);
        Task<Participant> AddParticipantAsync(ParticipantVm participantVm, string tournamentName);

        Task<List<Participant>> BulkAddParticipantsAsync(List<ParticipantVm> participantVms,
            string tournamentName);

        Task<Participant> CheckInParticipantAsync(string participantId, string tournamentName);
        Task<Participant> UndoCheckInParticipantAsync(string participantId, string tournamentName);
        Task<Participant> RandomizeParticipantsAsync(string tournamentName);
        Task<Participant> UpdateParticipantAsync(ParticipantVm participantVm, string tournamentName);
        Task<bool> RemoveParticipantAsync(string participantId, string tournamentName);
        Task<bool> ClearAllParticipants(string tournamentName);
    }
}