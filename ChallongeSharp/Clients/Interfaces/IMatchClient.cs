using System.Collections.Generic;
using System.Threading.Tasks;
using ChallongeSharp.Models.ChallongeModels;
using ChallongeSharp.Models.ViewModels.Types;

namespace ChallongeSharp.Clients
{
    public interface IMatchClient
    {
        Task<List<Match>> GetAllMatchesAsync(string tournamentName, TournamentState state = null,
            long? participantId = null);

        Task<Match> GetMatchAsync(string tournamentName, long matchId,
            bool includeAttachments = false);

        Task<Match> UpdateMatchAsync(string tournamentName, long matchId, int player1Score,
            int player2Score, int? player1Votes = null, int? player2Votes = null);

        Task<Match> ReopenMatchAsync(string tournamentName, long matchId);
    }
}